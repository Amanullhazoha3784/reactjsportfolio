import React, { Component } from 'react'
import classes from './header.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisH} from '@fortawesome/free-solid-svg-icons'
import { Container, Row, Col } from 'reactstrap'
import Menu from './menu'
import SocailLink from './socailLink'

class Header extends Component{

    state = {
        menuOpen: false,
    }

    toggleBtn = () => {
        this.setState({
            menuOpen: !this.state.menuOpen
        })
        console.log(this.state.menuOpen)
    }

    render() {
        const {header, logo, menubtn, active} = classes
        return (
            <header className={header}>
                <Container>
                    <Row>
                        <Col className='d-flex align-items-center justify-content-between'>
                            <div>
                                <h1 className={logo}><a href='#'>Amanullha</a></h1>
                                <SocailLink />
                            </div>
                            <Menu isOpen={this.state.menuOpen}/>
                            <button className={`d-block d-lg-none ${menubtn} ${this.state.menuOpen ? `${active}` : ''}`} onClick={this.toggleBtn}><FontAwesomeIcon icon={faEllipsisH}/></button>
                        </Col>
                    </Row>
                </Container>
            </header>
        )
    }
}

export default Header