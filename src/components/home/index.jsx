import React from 'react';
import Header from './header';
import Carousel from './carousel';

const Home = () => {
    return (
        <section>
            <Header />
            <Carousel />
        </section>
    )
}

export default Home