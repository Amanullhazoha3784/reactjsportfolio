import React from 'react'
import { Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

const PortfolioImage = ({imageData}) => {
    return imageData.map((image, index) =>(
        <Col md={3} key={index} name={image.value}>
            <figure>
                <img src={image.name} className='img-fluid'/>
                <figcaption className='d-flex justify-content-btween'>
                    <FontAwesomeIcon icon={faCoffee} />
                    <FontAwesomeIcon icon={faCoffee} />
                </figcaption>
            </figure>
        </Col>
    ))
}

export default PortfolioImage