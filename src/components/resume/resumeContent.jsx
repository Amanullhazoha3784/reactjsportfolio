import React from 'react'
import PropTypes from 'prop-types'
import classes from './resume.module.css'

const Content = ({ resume }) => {
    const {year, italicText, text, resumeContent, mainContent, resumeTitle} = classes
    return resume.map((r, index) => (
        <div key={index} className={resumeContent}>
            {r.head ? <h3 className={resumeTitle}>{r.head}</h3> : ''}
            <div className={mainContent}>
                <h5>{r.subHead}</h5>
                {r.years ? <p className={year}>{r.years}</p> : ''}
                {r.itlicText ? <p className={italicText}><em>{r.italicText}</em></p> : ''}
                {r.paraghap ? <p className={text}>{r.paraghap}</p> : ''}
                {r.list ? <ul>
                    {r.list.map((l, index) => (
                        <li key={index}>{l}</li>
                    ))}
                </ul> : ''}
            </div>
        </div>
    ))
}

Content.propTypes = {
    resume: PropTypes.array.isRequired
}

export default Content