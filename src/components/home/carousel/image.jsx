import React from 'react'
import PropTypes from 'prop-types'
import classes from './carousel.module.css'

const Img = ({ img }) => {
    const {sliderImg} = classes
    return <img src={img} className={`img-fluid ${sliderImg}`} alt='Slider Image' />
}

Img.propTypes = {
    img: PropTypes.string.isRequired
}

export default Img