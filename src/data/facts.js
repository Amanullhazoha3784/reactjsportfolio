import { faSmile, faFolderOpen, faHeadset, faUsers } from '@fortawesome/free-solid-svg-icons'

const FactsInfo = [
    {
        iconName: faSmile,
        vote: 225,
        iconTitle: 'Happy Clints',
        iconPara: 'consequuntur quae',
        style:{color: 'red'}
    },
    {
        iconName: faFolderOpen,
        vote: 515,
        iconTitle: 'Projects',
        iconPara: 'adipisci atque cum quia aut',
        style:{color: 'black'}
    },
    {
        iconName: faHeadset,
        vote: 1463,
        iconTitle: 'Hours Of Support',
        iconPara: 'aut commodi quaerat',
        style:{color: 'black'}
    },
    {
        iconName: faUsers,
        vote: 15,
        iconTitle: 'Hard Workers',
        iconPara: 'rerum asperiores dolor',
        style:{color: 'black'}
    }
]

export default FactsInfo