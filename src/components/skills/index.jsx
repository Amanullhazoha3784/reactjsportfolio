import React, { Component } from 'react'
import classes from './skills.module.css'
import Progresses from '../../data/skills'
import { Container, Row, Col } from 'reactstrap'
import SectionHeading from '../com/sectionHeader'
import Progres from './progres'

class Skills extends Component{
    state = {
        heading: 'Skills',
        para: 'Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.',
        progress: Progresses
    }

    render() {
        const {skills} = classes
        return (
            <section className={skills}>
                <Container>
                    <Row>
                        <Col md={8} className='m-auto'>
                            <SectionHeading
                                heading={this.state.heading}
                                paraghp={this.state.para}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Progres
                            prog={this.state.progress}
                        />
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Skills