import React from 'react'
import pic from '../../../img/carousel/p1.jpg'

const AboutLeft = () => {
    return <img src={pic} className='img-fluid'/>
}

export default AboutLeft