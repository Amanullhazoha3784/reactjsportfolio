import React, { Component } from 'react'
import classes from './portfolio.module.css'
import AllImagedata from '../../data/portfolio'
import { Container, Row, Col } from 'reactstrap'
import SectionHeading from '../com/sectionHeader'
import ControlBtn from './controlButton'
import PortfolioImage from './portfolioImg'

class Portfolio extends Component{
    state = {
        head: 'Portfolio',
        allImagedata: AllImagedata,
        updateItem: AllImagedata,
        buttonArray: ['all', 'app', 'card', 'web'],
        isActive: false,
    }

    componentDidMount() {
        this.initButton()
    }

    initButton = () => {
        this.state.buttonArray.forEach(button => {
            document.getElementById(button).classList.remove('activeButton')
            document.getElementById(button).classList.add('inActiveButton')
        })
    }

    handelAll = (id) => {
       this.initButton()

        document.getElementById(id).classList.remove('activeButton')
        document.getElementById(id).classList.add('inActiveButton')

        this.setState({
            updateItem: AllImagedata,
            isActive: true
        })
    }

    handelChange = (catItem, id) => {
        const updateItem = this.state.allImagedata.filter((curItem) => {
            return curItem.value === catItem
        })
        
        this.initButton()

        document.getElementById(id).classList.remove('activeButton')
        document.getElementById(id).classList.add('inActiveButton')
        
        this.setState({
            updateItem: updateItem,
            isActive: !this.state.isActive
        })
    }

    getVeiw = () => {
        return <PortfolioImage
            imageData={this.state.updateItem}
        />
    }

    render() {
        const { portfolio } = classes
        return(
            <section className={portfolio}>
                <Container>
                    <Row>
                        <Col md={8} className='m-auto'>
                            <SectionHeading
                                heading={this.state.head}
                            />
                            <Col className='d-flex justify-content-center'>
                                <ControlBtn
                                    handelAll={this.handelAll}
                                    handelChange={this.handelChange}
                                    isActive={this.state.isActive}
                                />
                            </Col>
                        </Col>
                    </Row>
                    <Row>
                        {this.getVeiw()}
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Portfolio