import React from 'react'
import classes from './portfolio.module.css'
import PropTypes from 'prop-types'

const ControlBtn = ({ handelChange, handelAll, isActive }) => {
    return (
        <ul>
            <li onClick={() => handelAll('all', 'all')} id='all' className={isActive ? 'activeButton' : 'inActiveButton'}>All</li>
            <li onClick={() => handelChange('app', 'app')} id='app' className={!isActive ? 'activeButton' : 'inActiveButton'}>App</li>
            <li onClick={() => handelChange('card', 'card')} id='card' className={!isActive ? 'activeButton' : 'inActiveButton'}>Card</li>
            <li onClick={() => handelChange('web', 'web')} id='web' className={!isActive ? 'activeButton' : 'inActiveButton'}>Web</li>
        </ul>
    )
}

ControlBtn.propTypes = {
    handelAll: PropTypes.func.isRequired,
    handelChange: PropTypes.func.isRequired
}

export default ControlBtn