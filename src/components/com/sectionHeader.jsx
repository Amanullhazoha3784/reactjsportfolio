import React from 'react'
import PropTypes from 'prop-types'
import classess from './sectionHeading.module.css'

const SectionHeading = ({ heading, paraghp }) => {
    const {sectionHeading} = classess
    return (
        <div className={sectionHeading}>
            <h2 className='text-center'>{heading}</h2>
            <p className='text-center'>{paraghp}</p>
        </div>
    )
}

SectionHeading.propTypes = {
    heading: PropTypes.string.isRequired,
    paraghp: PropTypes.string
}

export default SectionHeading