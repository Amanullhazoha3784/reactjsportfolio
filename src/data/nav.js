import shortid from 'shortid'

const NAV = [
    { id: shortid.generate(), value: 'Home' },
    { id: shortid.generate(), value: 'About' },
    { id: shortid.generate(), value: 'Contact' },
    { id: shortid.generate(), value: 'Gallery' },
    { id: shortid.generate(), value: 'Help' },
]

export default NAV