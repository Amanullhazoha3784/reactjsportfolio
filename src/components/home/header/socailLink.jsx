import React from 'react'
import classes from './header.module.css'
import { Nav } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faTwitter, faInstagram, faLinkedinIn, faPinterestP } from '@fortawesome/free-brands-svg-icons'

const SocailLink = () => {
    const {socailLink, facebook, twitter, instagram, linkedin, pinterest} = classes
    return (
<Nav className={`d-flex justify-content-center ${socailLink}`}>
            <li><a href='#' className={facebook}><FontAwesomeIcon icon={faFacebookF} /></a></li>
            <li><a href='#' className={twitter}><FontAwesomeIcon icon={faTwitter} /></a></li>
            <li><a href='#' className={instagram}><FontAwesomeIcon icon={faInstagram} /></a></li>
            <li><a href='#' className={linkedin}><FontAwesomeIcon icon={faLinkedinIn} /></a></li>
            <li><a href='#' className={pinterest}><FontAwesomeIcon icon={faPinterestP} /></a></li>
        </Nav>
    )
}

export default SocailLink