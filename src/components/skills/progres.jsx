import React from 'react'
import classes from './skills.module.css'
import { Progress, Col } from 'reactstrap';

const Progres = ({ prog }) => {
    return prog.map((progress, index) => {
        const {progressStyle, progressBar} = classes
        return (
            <Col md={6} key={index} className={progressBar}>
                <div className='d-flex justify-content-between'><span>{progress.name}</span><span>{progress.value}%</span></div>
                <Progress value={progress.value} className={progressStyle}/>
            </Col>
        )
    })
}

export default Progres