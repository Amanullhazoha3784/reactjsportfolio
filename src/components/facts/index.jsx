import React, { Component } from 'react'
import classes from './facts.module.css'
import { Container, Row, Col } from 'reactstrap'
import FactsInfo from '../../data/facts'
import SectionHeading from '../com/sectionHeader'
import FactIcon from './factIcon'

class Facts extends Component{
    state = {
        heading: 'Facts',
        secPara: 'Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.',
        factsValue: FactsInfo
    }
    render() {
        const {facts} = classes
        return (
            <section className={facts}>
                <Container>
                    <Row>
                        <Col md={8} className='m-auto'>
                            <SectionHeading
                                heading={this.state.heading}
                                paraghp={this.state.secPara}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <FactIcon
                            factsInfo={this.state.factsValue}
                        />
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Facts