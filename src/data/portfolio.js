import image1 from '../img/portfolio/image1.jpg'
import image2 from '../img/portfolio/image2.jpg'
import image3 from '../img/portfolio/image3.jpg'
import image4 from '../img/portfolio/image4.jpg'
import image5 from '../img/portfolio/image5.jpg'
import image6 from '../img/portfolio/image6.jpg'
import image7 from '../img/portfolio/image7.jpg'
import image8 from '../img/portfolio/image8.jpg'

const AllImagedata = [
    {
        name: image1,
        value: 'app'
    },
    {
        name: image2,
        value: 'card'
    },
    {
        name: image3,
        value: 'web'
    },
    {
        name: image4,
        value: 'app'
    },
    {
        name: image5,
        value: 'card'
    },
    {
        name: image6,
        value: 'app'
    },
    {
        name: image7,
        value: 'web'
    },
    {
        name: image8,
        value: 'app'
    },
]

export default AllImagedata