import React, { Component } from 'react'
import classes from './carousel.module.css'
import { Container, Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleLeft, faChevronCircleRight } from '@fortawesome/free-solid-svg-icons'
import Img from './image'
import p1 from '../../../img/carousel/p1.jpg'
import p2 from '../../../img/carousel/p2.jpg'
import p3 from '../../../img/carousel/p3.jpg'
import p4 from '../../../img/carousel/p4.jpg'
import p5 from '../../../img/carousel/p5.jpg'

const sliderArr = [<Img img={p1}/>,<Img img={p2}/>,<Img img={p3}/>,<Img img={p4}/>,<Img img={p5}/>]

class Carousel extends Component{
    state = {
        x: 0,
        autoPlay: true
    }

    goLeft = () => {
        this.state.x === 0 ? this.setState({x: -100 * (sliderArr.length -1)}) : this.setState({x: this.state.x + 100})
    }

    goRight = () => {
        this.state.x === -100 * (sliderArr.length - 1) ? this.setState({ x: 0 }) : this.setState({ x: this.state.x - 100 })
    }

    /*autoPlay = () => {
        setTimeout(() => {
            this.setState({x: this.state.x + 100})
        }, 3000);
    }*/
    
    render() {   
        const { carousel, slider, item, goLeft, goRight } = classes
        return (
            <section className={carousel}>
                <Container>
                    <Row>
                        <Col>
                            <div className={slider} >
                                {sliderArr.map((sliderItem, index) => (
                                    <div key={index} className={item} style={{transform: `translateX(${this.state.x}%)`}}>
                                        {sliderItem}
                                    </div>
                                ))}
                                <button id={goLeft} onClick={this.goLeft}><FontAwesomeIcon icon={faChevronCircleLeft}/></button>
                                <button id={goRight} onClick={this.goRight}><FontAwesomeIcon icon={faChevronCircleRight}/></button>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Carousel