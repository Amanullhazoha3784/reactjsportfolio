import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import './App.css'
import Home from './home'
import About from './about'
import Facts from './facts'
import Skills from './skills'
import Resume from './resume'
import Portfolio from './portfolio'

class App extends Component{

    render() {
        return (
            <div>
                <Home />
                <About />
                <Facts />
                <Skills />
                <Resume />
                <Portfolio />
            </div>
        )
    }
}

export default App