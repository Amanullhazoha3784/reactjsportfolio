import React, { Component } from 'react'
import classes from './resume.module.css'
import {ResumeDetailsLeft, ResumeDetailsRight} from '../../data/resume'
import { Container, Row, Col } from 'reactstrap'
import SectionHeading from '../com/sectionHeader'
import Content from './resumeContent'

class Resume extends Component{
    state = {
        heading: 'Resume',
        para: 'Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.',
        resumeDetailsLeft: ResumeDetailsLeft,
        resumeDetailsRight: ResumeDetailsRight
    }

    render() {
        const {resume} = classes
        return (
            <section className={resume}>
                <Container>
                    <Row>
                        <Col md={8} className='m-auto'>
                            <SectionHeading
                                heading={this.state.heading}
                                paraghp={this.state.para}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <Content
                                resume={this.state.resumeDetailsLeft}
                            />
                        </Col>
                        <Col md={6}>
                            <Content
                                resume={this.state.resumeDetailsRight}
                            />
                        </Col>
                    </Row>
                </Container>
            </section>
        )
    }
}

export default Resume