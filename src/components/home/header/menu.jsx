import React from 'react'
import PropTypes from 'prop-types'
import classes from './header.module.css'
import { Nav, Navbar } from 'reactstrap'


const Menu = ({isOpen}) => {
    const {menubar, menuActive} = classes
    return (
        <Navbar className={`d-lg-block ${isOpen ? `${menuActive}` : 'd-none'}`}>
            <Nav className={menubar}>
                <li><a href='#'>Home</a></li>
                <li><a href='#'>About</a></li>
                <li><a href='#'>Resume</a></li>
                <li><a href='#'>Portfolio</a></li>
                <li><a href='#'>Services</a></li>
                <li><a href='#'>Contact</a></li>
            </Nav>
        </Navbar>
    )
}

Menu.propTypes = {
    isOpen: PropTypes.bool.isRequired
}

export default Menu