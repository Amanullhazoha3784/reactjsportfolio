import React from 'react'
import classes from './facts.module.css'
import { Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const FactIcon = ({ factsInfo }) => {
    const {content, iconStyle} = classes
    return factsInfo.map((fact, index) => {
        return (
            <Col md={3} xs={6} key={index}>
                <div className={content}>
                    <FontAwesomeIcon icon={fact.iconName} className={iconStyle} style={ fact.style}/>
                    <h3>{fact.vote}</h3>
                    <h4>{fact.iconTitle}</h4>
                    <p>{fact.iconPara}</p>
                </div>
            </Col>
        )
    })
}

export default FactIcon