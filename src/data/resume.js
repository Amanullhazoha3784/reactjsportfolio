const ResumeDetailsLeft = [
    {
        head: 'Sumary',
        subHead: 'Amanullha Zoha',
        paraghap: 'Innovative and deadline-driven Graphic Designer with 3+ years of experience designing and developing user-centered digital/print marketing material from initial concept to final, polished deliverable',
        list: [
            'Portland par 127,Orlando, FL',
            '(123) 456-7891',
            'alice.barkley@example.com'
        ]
    },
    {
        head: 'Education',
        subHead: 'MASTER OF FINE ARTS & GRAPHIC DESIGN',
        years: '2015 - 2016',
        itlicText: 'Rochester Institute of Technology, Rochester, NY',
        paraghap: 'Qui deserunt veniam. Et sed aliquam labore tempore sed quisquam iusto autem sit. Ea vero voluptatum qui ut dignissimos deleniti nerada porti sand markend'
    },
    {
        subHead: 'BACHELOR OF FINE ARTS & GRAPHIC DESIGN',
        years: '2010 - 2014',
        itlicText: 'Rochester Institute of Technology, Rochester, NY',
        paraghap: 'Quia nobis sequi est occaecati aut. Repudiandae et iusto quae reiciendis et quis Eius vel ratione eius unde vitae rerum voluptates asperiores voluptatem Earum molestiae consequatur neque etlon sader mart dila'
    }  
]

const ResumeDetailsRight = [
    {
        head: 'Professional Experience',
        subHead: 'SENIOR GRAPHIC DESIGN SPECIALIST',
        years: '2019 - Present',
        itlicText: 'Experion, New York, NY',
        list: [
            'Lead in the design, development, and implementation of the graphic, layout, and production communication materials',
            'Delegate tasks to the 7 members of the design team and provide counsel on all aspects of the project.',
            'Supervise the assessment of all graphic materials in order to ensure quality and accuracy of the design',
            'Oversee the efficient use of production project budgets ranging from $2,000 - $25,000'
        ]
    },
    {
        subHead: 'GRAPHIC DESIGN SPECIALIST',
        years: '2017 - 2018',
        itlicText: 'Stepping Stone Advertising, New York, NY',
        list: [
            'Developed numerous marketing programs (logos, brochures,infographics, presentations, and advertisements).',
            'Managed up to 5 projects or tasks at a given time while under pressure',
            'Recommended and consulted with clients on the most appropriate graphic design',
            'Created 4+ design presentations and proposals a month for clients and account managers'
        ]
    },
]



export { ResumeDetailsLeft, ResumeDetailsRight }