import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'
import SectionHeading from '../com/sectionHeader'
import AboutLeft from './aboutLeft'
import AboutRight from './aboutRight'

class About extends Component{
    state = {
        heading: 'About',
        secPara: 'Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.'
    }

    render() {
        return (
            <section className='about section'>
                <Container>
                    <Row>
                        <Col className='m-auto' md={{ size: 9 }}>
                            <SectionHeading
                                heading={this.state.heading}
                                paraghp={this.state.secPara}
                            />
                        </Col>
                    </Row>
                    <div className='content-section'>
                        <Row>
                            <Col className='d-none d-lg-block' md={4}>
                                <AboutLeft />
                            </Col>
                            <AboutRight />
                        </Row>
                    </div>
                </Container>
            </section>
        )
    }
}

export default About