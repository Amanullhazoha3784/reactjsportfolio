const PersonDetails = [
    { title: 'Birthday', value: '9 April 2001' },
    { title: 'Website', value: 'www.example.com' },
    { title: 'Phone', value: '+880 1754 310 604' },
    { title: 'City', value: 'Dhaka, Bangladesh' },
    { title: 'Age', value: '20' },
    { title: 'Degree', value: 'BA' },
    { title: 'Email', value: 'jahdbd9x@gmail.com' },
    { title: 'Freelance', value: 'Available' }  
]

export default PersonDetails