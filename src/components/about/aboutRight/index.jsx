import React, { Component } from 'react'
import { Col } from 'reactstrap'
import PersonDetails from '../../../data/personDetails'
import classes from './aboutRight.module.css'

class AboutRight extends Component{
    state = {
        personDetails: []
    }

    componentDidMount() {
        this.setState({
            personDetails: PersonDetails
        })
    }  

    render() {
        const {aboutRight} = classes
        return (
            <Col md={8} className={aboutRight}>
                <h3>Web Designer & Web Devloper</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <ul>
                    {this.state.personDetails.map((person, index) => {
                        return <li key={index}><span>{person.title}:</span> {person.value}</li>
                    })}
                </ul>
                <p>Officiis eligendi itaque labore et dolorum mollitia officiis
                    optio vero. Quisquam sunt adipisci omnis et ut. Nulla
                    accusantium dolor incidunt officia tempore. Et eius omnis.
                    Cupiditate ut dicta maxime officiis quidem quia. Sed et
                    consectetur qui quia repellendus itaque neque. Aliquid
                    amet quidem ut quaerat cupiditate. Ab et eum qui repellendus
                    omnis culpa magni laudantium dolores.
                </p>
            </Col>
        )
    }
}

export default AboutRight